package com.stepanov.patterns.behavioral.strategy.developer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sleeping implements Activity {
    private final Logger log = LoggerFactory.getLogger(Sleeping.class);

    @Override
    public void justDoIt() {
        log.info("Sleeping...");
    }
}
