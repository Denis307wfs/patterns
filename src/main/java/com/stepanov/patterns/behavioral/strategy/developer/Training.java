package com.stepanov.patterns.behavioral.strategy.developer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Training implements Activity {
    private final Logger log = LoggerFactory.getLogger(Training.class);

    @Override
    public void justDoIt() {
        log.info("Training...");
    }
}
