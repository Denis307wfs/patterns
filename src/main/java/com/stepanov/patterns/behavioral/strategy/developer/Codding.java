package com.stepanov.patterns.behavioral.strategy.developer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Codding implements Activity {
    private final Logger log = LoggerFactory.getLogger(Codding.class);

    @Override
    public void justDoIt() {
        log.info("Writing code...");
    }
}
