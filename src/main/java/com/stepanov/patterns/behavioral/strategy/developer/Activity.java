package com.stepanov.patterns.behavioral.strategy.developer;

public interface Activity {
    void justDoIt();
}
