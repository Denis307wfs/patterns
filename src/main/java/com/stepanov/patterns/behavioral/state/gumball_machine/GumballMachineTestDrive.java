package com.stepanov.patterns.behavioral.state.gumball_machine;

public class GumballMachineTestDrive {

    public static void main(String[] args) {
        GumballMachine gumballMachine = new GumballMachine(2);

        System.out.println(gumballMachine.getState());

        gumballMachine.ejectQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());
        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.turnCrank();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.ejectQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.turnCrank();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.addGumball(1);
        System.out.println(gumballMachine.getCount());
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.addGumball(2);
        System.out.println(gumballMachine.getCount());
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.ejectQuarter();
        System.out.println(gumballMachine.getState());

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        System.out.println(gumballMachine.getState());
        gumballMachine.turnCrank();

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println("----------------------------------");

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println("----------------------------------");
    }
}
