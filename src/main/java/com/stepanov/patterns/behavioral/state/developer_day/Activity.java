package com.stepanov.patterns.behavioral.state.developer_day;

public interface Activity {
    void justDoIt();
}
