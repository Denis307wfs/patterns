package com.stepanov.patterns.behavioral.state.developer_day;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reading implements Activity {
    private final Logger log = LoggerFactory.getLogger(Reading.class);

    @Override
    public void justDoIt() {
        log.info("Reading book...");
    }
}
