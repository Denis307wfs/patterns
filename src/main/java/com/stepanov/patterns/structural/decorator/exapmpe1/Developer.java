package com.stepanov.patterns.structural.decorator.exapmpe1;

public interface Developer {
    String makeJob();
}
