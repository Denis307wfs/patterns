package com.stepanov.patterns.structural.decorator.exapmpe1;

public class JavaDeveloper implements Developer {
    @Override
    public String makeJob() {
        return "Write Java code. ";
    }
}
