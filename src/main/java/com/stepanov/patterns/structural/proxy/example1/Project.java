package com.stepanov.patterns.structural.proxy.example1;

public interface Project {
    void run();
}
