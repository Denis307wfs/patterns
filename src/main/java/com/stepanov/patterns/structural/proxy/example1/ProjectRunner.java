package com.stepanov.patterns.structural.proxy.example1;

public class ProjectRunner {
    private static String url = "https://www.hithub.com/username/realProject";

    public static void main(String[] args) {
//        Project project = new RealProject(url);
        Project project = new ProxyProject(url);

        project.run();
    }
}
