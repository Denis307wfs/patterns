package com.stepanov.patterns.structural.bridge.remote_device;

import com.stepanov.patterns.structural.bridge.remote_device.devices.Device;
import com.stepanov.patterns.structural.bridge.remote_device.devices.Radio;
import com.stepanov.patterns.structural.bridge.remote_device.devices.Tv;
import com.stepanov.patterns.structural.bridge.remote_device.remotes.AdvancedRemote;
import com.stepanov.patterns.structural.bridge.remote_device.remotes.BasicRemote;

public class Demo {
    public static void main(String[] args) {
        testDevice(new Tv());
        testDevice(new Radio());
    }

    public static void testDevice(Device device) {
        System.out.println("Tests with basic remote.");
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Tests with advanced remote.");
        AdvancedRemote advancedRemote = new AdvancedRemote(device);
        advancedRemote.power();
        advancedRemote.mute();
        device.printStatus();
    }
}