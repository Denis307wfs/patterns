package com.stepanov.patterns.structural.bridge.remote_device.remotes;

public interface Remote {
    void power();

    void volumeDown();

    void volumeUp();

    void channelDown();

    void channelUp();
}