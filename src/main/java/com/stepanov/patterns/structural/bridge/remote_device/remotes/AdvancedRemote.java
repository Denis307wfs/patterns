package com.stepanov.patterns.structural.bridge.remote_device.remotes;

import com.stepanov.patterns.structural.bridge.remote_device.devices.Device;

public class AdvancedRemote extends BasicRemote {

    public AdvancedRemote(Device device) {
        super.device = device;
    }

    public void mute() {
        System.out.println("Remote: mute");
        device.setVolume(0);
    }
}